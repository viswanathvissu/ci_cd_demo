#!/bin/bash

# any future command that fails will exit the script


# Delete the old repo
set -e

ls



rm -rf sapnodejs
# clone the repo again
git clone https://github.com/vissunani/sapnodejs.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH



# stop the previous pm2
pm2 kill
npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
npm install pm2 -g
# starting pm2 daemon
pm2 status

cd /home/ec2-user/sapnodejs



#install npm packages
echo "Running npm install"
npm install

#Restart the node server
npx kill-port 3000

pwd

bash build.sh